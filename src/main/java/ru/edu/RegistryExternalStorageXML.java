package ru.edu;

import ru.edu.model.Registry;

import java.io.IOException;

public class RegistryExternalStorageXML implements RegistryExternalStorage {

    /**
     * Registry class variable.
     */
    private Registry registry = new Registry();

    /**
     * Empty constructor.
     */
    public RegistryExternalStorageXML() {
    }

    /**
     * Constructor.
     * @param reg Registry object
     */
    public RegistryExternalStorageXML(final Registry reg) {
        this.registry = reg;
    }

    /**
     * Чтение из файла.
     *
     * @param filePath путь до файла
     */
    @Override
    public void readFrom(final String filePath) throws IOException {
        registry = MapperUtils.readXml(filePath, Registry.class);
    }

    /**
     * Запись реестра в файл.
     *
     * @param filePath путь
     * @param reg реестр
     */
    @Override
    public void writeTo(final String filePath,
                        final Registry reg) throws IOException {
        MapperUtils.mapXml(filePath, reg);
    }

    /**
     * Запись реестра в файл.
     *
     * @param filePath путь
     */
    @Override
    public void writeTo(final String filePath) throws IOException {
        writeTo(filePath, registry);
    }

    /**
     * Get Registry object.
     * @return Registry object
     */
    public Registry getRegistry() {
        return registry;
    }

    /**
     * Set Registry object.
     * @param reg Registry object
     */
    public void setRegistry(final Registry reg) {
        this.registry = reg;
    }

}
