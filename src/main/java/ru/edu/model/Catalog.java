package ru.edu.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.ArrayList;
import java.util.List;

@JacksonXmlRootElement(localName = "CATALOG")
public class Catalog {

    /**
     * List of CDs.
     */
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "CD")
    private List<CD> cdList = new ArrayList<>();

    /**
     * Get CD's list.
     * @return List of CDs
     */
    public List<CD> getCdList() {
        return cdList;
    }

    /**
     * Add CD object to CD's list.
     * @param theCD CD object
     */
    public void addCD(final CD theCD) {
        cdList.add(theCD);
    }

    /**
     * Empty constructor.
     */
    public Catalog() {

    }

    /**
     * Construct Catalog from CDs list.
     * @param cdsList List of CD objects
     */
    public Catalog(final List<CD> cdsList) {
        this.cdList = cdsList;
    }


}
