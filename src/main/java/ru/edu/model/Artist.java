package ru.edu.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Artist implements Serializable {

    /**
     * Artist name.
     */
    @JacksonXmlProperty(localName = "Name")
    private String name;

    /**
     * Artist albums.
     */
    @JacksonXmlElementWrapper(localName = "Albums")
    @JacksonXmlProperty(localName = "Album")
    private final List<Album> albums = new ArrayList<>();

    /**
     * Construct Artist with name.
     * @param artistName Artist name
     */
    public Artist(final String artistName) {
        this.name = artistName;
    }

    /**
     * Empty constructor.
     */
    public Artist() {
    }

    /**
     * Get Artist name.
     * @return String with artist name
     */
    public String getName() {
        return name;
    }

    /**
     * Get Artist albums.
     * @return List of albums
     */
    public List<Album> getAlbums() {
        return albums;
    }

    /**
     * Add album to Artist albums list.
     * @param album Album object
     */
    public void addAlbum(final Album album) {
        albums.add(album);
    }

    /**
     * Print Artist object.
     * @return String with Artist info
     */
    @Override
    public String toString() {
        return "Artist{"
                + "name='" + name + '\''
                + ", albums=" + albums
                + "}\n";
    }
}
