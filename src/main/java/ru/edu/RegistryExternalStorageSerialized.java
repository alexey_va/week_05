package ru.edu;

import ru.edu.model.Registry;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class RegistryExternalStorageSerialized
        implements RegistryExternalStorage {

    /**
     * Registry class variable.
     */
    private Registry registry = new Registry();

    /**
     * Empty constructor.
     */
    public RegistryExternalStorageSerialized() {
    }

    /**
     * Constructor.
     * @param reg Registry object
     */
    public RegistryExternalStorageSerialized(final Registry reg) {
        this.registry = reg;
    }

    /**
     * Чтение из файла.
     *
     * @param filePath путь до файла
     */
    @Override
    public void readFrom(final String filePath) throws IOException {

        FileInputStream fileInputStream = new FileInputStream(filePath);
        ObjectInputStream objectInputStream =
                new ObjectInputStream(fileInputStream);

        try {
            registry = (Registry) objectInputStream.readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


    }

    /**
     * Запись реестра в файл.
     *
     * @param filePath путь
     * @param reg реестр
     */
    @Override
    public void writeTo(final String filePath,
                        final Registry reg) throws IOException {

        FileOutputStream fous = new FileOutputStream(filePath);
        ObjectOutputStream objectOutputStream =
                new ObjectOutputStream(fous);
        objectOutputStream.writeObject(reg);

    }

    /**
     * Запись реестра в файл.
     *
     * @param filePath путь
     */
    @Override
    public void writeTo(final String filePath) throws IOException {
        writeTo(filePath, registry);
    }

    /**
     * Get Registry object.
     * @return Registry object
     */
    public Registry getRegistry() {
        return registry;
    }

    /**
     * Set Registry object.
     * @param reg Registry object
     */
    public void setRegistry(final Registry reg) {
        this.registry = reg;
    }


}
