package ru.edu.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import ru.edu.MapperUtils;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static ru.edu.model.CatalogTest.EXAMPLE_FILES_DIR;

public class ArtistTest {

    @Test
    public void testGetNameJson() throws IOException {

        Artist artist = MapperUtils.readJson(EXAMPLE_FILES_DIR + "artist.json", Artist.class);

        // Замена в строку
        String jsonObject = new ObjectMapper().writeValueAsString(artist);
        assertEquals ("{\"name\":\"ArtistName\",\"albums\":[]}", jsonObject);
        assertEquals ("ArtistName", artist.getName());

    }


    @Test
    public void testGetNameXml() throws IOException {

        Artist artist = MapperUtils.readXml(EXAMPLE_FILES_DIR + "artist.xml", Artist.class);

        String jsonObject = new ObjectMapper().writeValueAsString(artist);
        assertEquals ("{\"name\":\"ArtistName\",\"albums\":[]}", jsonObject);
        assertEquals ("ArtistName", artist.getName());

    }

}