package ru.edu.model;

import org.junit.Test;
import ru.edu.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Date;

import static org.junit.Assert.*;

public class CatalogToRegistryTest {

    public static final String INPUT_CD_CATALOG_XML = "./input/cd_catalog.xml";
    public static final String OUTPUT_CATALOG_JSON = "./output/artist_by_country.json";
    public static final String OUTPUT_CATALOG_XML = "./output/artist_by_country.xml";
    public static final String OUTPUT_CATALOG_BINARY = "./output/artist_by_country.serialized";

    Catalog catalog = null;
    Registry registry = null;

    @Test
    public void testReadCatalog() throws IOException {

        catalog = MapperUtils.readXml(INPUT_CD_CATALOG_XML, Catalog.class);

        assertTrue(catalog.getCdList().size() > 0);
        assertNotNull(catalog.getCdList().get(0).getName());
        assertNotNull(catalog.getCdList().get(0).getCountry());
        assertNotNull(catalog.getCdList().get(0).getTitle());
        assertTrue(catalog.getCdList().get(0).getYear() > 0);

    }

    @Test
    public void testTransform() throws IOException {

        catalog = MapperUtils.readXml(INPUT_CD_CATALOG_XML, Catalog.class);
        registry = new TransformCatalogToRegistry(catalog).transform();

        assertTrue(registry.getCountryCount() > 0);
        assertTrue(registry.getCountries().size() > 0);
        assertTrue(registry.getCountries().get(0).getArtists().size() > 0);
        assertTrue(registry.getCountries().get(0).getArtists().get(0).getAlbums().size() > 0);

        System.out.println(registry);
    }

    @Test
    public void testWriteToXML() throws IOException {

        catalog = MapperUtils.readXml(INPUT_CD_CATALOG_XML, Catalog.class);
        registry = new TransformCatalogToRegistry(catalog).transform();
        new RegistryExternalStorageXML(registry).writeTo(OUTPUT_CATALOG_XML);

        Path p = Paths.get(OUTPUT_CATALOG_XML);
        assertEquals(System.currentTimeMillis() / 100, Files.getLastModifiedTime(p).toMillis() / 100);

    }

    @Test
    public void testWriteToJSON() throws IOException {

        catalog = MapperUtils.readXml(INPUT_CD_CATALOG_XML, Catalog.class);
        registry = new TransformCatalogToRegistry(catalog).transform();
        new RegistryExternalStorageJSON(registry).writeTo(OUTPUT_CATALOG_JSON);

        Path p = Paths.get(OUTPUT_CATALOG_JSON);
        long a1, b1;
        a1 = (long) System.currentTimeMillis() / 100;
        b1 = (long)  Files.getLastModifiedTime(p).toMillis() / 100;
        System.out.println(a1 + " " + b1);
        assertEquals(a1, b1);
    }

    @Test
    public void testWriteBinary() throws IOException {

        catalog = MapperUtils.readXml(INPUT_CD_CATALOG_XML, Catalog.class);
        registry = new TransformCatalogToRegistry(catalog).transform();
        new RegistryExternalStorageSerialized(registry).writeTo(OUTPUT_CATALOG_BINARY);

        Path p = Paths.get(OUTPUT_CATALOG_BINARY);
        assertEquals(System.currentTimeMillis() / 100, Files.getLastModifiedTime(p).toMillis() / 100);

    }

}