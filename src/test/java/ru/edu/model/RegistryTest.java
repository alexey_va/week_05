package ru.edu.model;

import org.junit.Test;
import ru.edu.MapperUtils;
import ru.edu.RegistryExternalStorageJSON;
import ru.edu.RegistryExternalStorageSerialized;
import ru.edu.RegistryExternalStorageXML;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class RegistryTest {

    public static final String OUTPUT_TEST_SERIALIZED = "src/test/resources/output/artist_by_country.serialized";
    public static final String OUTPUT_TEST_XML = "src/test/resources/output/artist_by_country.xml";
    public static final String OUTPUT_TEST_JSON = "src/test/resources/output/artist_by_country.json";
    public static final String OUTPUT_TEST_SERIALIZED_FILENOTFOUND = "./resources/output/file_by_country.serialized";
    public static final String OUTPUT_TEST_XML_FILENOTFOUND = "./resources/output/file_by_country.xml";
    public static final String OUTPUT_TEST_JSON_FILENOTFOUND = "./resources/output/file_by_country.json";

    RegistryExternalStorageXML registryExternalStorageXML =
            new RegistryExternalStorageXML();
    RegistryExternalStorageJSON registryExternalStorageJSON =
            new RegistryExternalStorageJSON();
    RegistryExternalStorageSerialized registryExternalStorageSerialized =
            new RegistryExternalStorageSerialized();

    Registry registry;

    @Test(expected = IOException.class)
    public void testWriteXmlIOException() throws IOException {

        registryExternalStorageXML.writeTo(OUTPUT_TEST_XML_FILENOTFOUND);

    }

    @Test
    public void testWriteXml() throws IOException {

        registry = registryExternalStorageXML.getRegistry();
        fillCountries(registry.getCountries());
        registryExternalStorageXML.writeTo(OUTPUT_TEST_XML);

    }

    @Test
    public void testWrite2Xml() throws IOException {

        registry = new Registry();
        fillCountries(registry.getCountries());
        registryExternalStorageXML.setRegistry(registry);
        registryExternalStorageXML.writeTo(OUTPUT_TEST_XML);

    }

    @Test
    public void testWrite3Xml() throws IOException {

        registry = new Registry();
        fillCountries(registry.getCountries());
        new RegistryExternalStorageXML(registry).writeTo(OUTPUT_TEST_XML);

    }

    @Test(expected = IOException.class)
    public void testWriteJsonIOException() throws IOException {

        registryExternalStorageJSON.writeTo(OUTPUT_TEST_JSON_FILENOTFOUND);

    }

    @Test
    public void testWriteJson() throws IOException {

        registry = registryExternalStorageJSON.getRegistry();
        fillCountries(registry.getCountries());
        registryExternalStorageJSON.writeTo(OUTPUT_TEST_JSON);

    }

    @Test
    public void testWrite2Json() throws IOException {

        registry = new Registry();
        fillCountries(registry.getCountries());
        registryExternalStorageJSON.setRegistry(registry);
        registryExternalStorageJSON.writeTo(OUTPUT_TEST_JSON);

    }

    @Test
    public void testWrite3Json() throws IOException {

        registry = new Registry();
        fillCountries(registry.getCountries());
        new RegistryExternalStorageJSON(registry).writeTo(OUTPUT_TEST_JSON);

    }


    @Test(expected = IOException.class)
    public void testWriteBinaryIOException() throws IOException {

        registryExternalStorageSerialized
                .writeTo(OUTPUT_TEST_SERIALIZED_FILENOTFOUND);

    }

    @Test
    public void testWriteBinary() throws IOException {

        registry = registryExternalStorageSerialized.getRegistry();
        fillCountries(registry.getCountries());
        registryExternalStorageSerialized
                .writeTo(OUTPUT_TEST_SERIALIZED);

    }

    @Test
    public void testWrite2Binary() throws IOException {

        registry = new Registry();
        fillCountries(registry.getCountries());
        registryExternalStorageSerialized.setRegistry(registry);
        registryExternalStorageSerialized
                .writeTo(OUTPUT_TEST_SERIALIZED);

    }

    @Test
    public void testWrite3Binary() throws IOException {

        registry = new Registry();
        fillCountries(registry.getCountries());
        new RegistryExternalStorageSerialized(registry)
                .writeTo(OUTPUT_TEST_SERIALIZED);

    }

    @Test(expected = IOException.class)
    public void testReadXmlIOException() throws IOException {

        registryExternalStorageXML.readFrom(OUTPUT_TEST_XML_FILENOTFOUND);

    }

    @Test
    public void testReadXml() throws IOException {

        registryExternalStorageXML.readFrom(OUTPUT_TEST_XML);
        registry = registryExternalStorageXML.getRegistry();

        assertEquals(2, registry.getCountryCount());
        assertEquals(2, registry.getCountries().get(0).getArtists().size());
        assertEquals(2, registry.getCountries().get(0).getArtists().get(0).getAlbums().size());

    }

    @Test(expected = IOException.class)
    public void testReadJsonIOException() throws IOException {

        registryExternalStorageJSON.readFrom(OUTPUT_TEST_JSON_FILENOTFOUND);

    }

    @Test
    public void testReadJson() throws IOException {

        registryExternalStorageJSON.readFrom(OUTPUT_TEST_JSON);
        registry = registryExternalStorageJSON.getRegistry();

        assertEquals(2, registry.getCountryCount());
        assertEquals(2, registry.getCountries().get(0).getArtists().size());
        assertEquals(2, registry.getCountries().get(0).getArtists().get(0).getAlbums().size());

    }

    @Test(expected = IOException.class)
    public void testReadBinaryIOException() throws IOException, ClassNotFoundException {

        registryExternalStorageSerialized.readFrom(OUTPUT_TEST_SERIALIZED_FILENOTFOUND);

    }

    @Test
    public void testReadBinary() throws IOException, ClassNotFoundException {

        registryExternalStorageSerialized.readFrom(OUTPUT_TEST_SERIALIZED);
        registry = registryExternalStorageSerialized.getRegistry();

        assertEquals(2, registry.getCountryCount());
        assertEquals(2, registry.getCountries().get(0).getArtists().size());
        assertEquals(2, registry.getCountries().get(0).getArtists().get(0).getAlbums().size());

    }


    private void fillCountries(List<Country> countries) {
        for (int i = 0; i < 2; i++) {
            Country country = new Country("Country " + i);
            fillArtist(country.getArtists());
            countries.add(country);
        }
    }


    private void fillArtist(List<Artist> artists) {
        for (int i = 0; i < 2; i++) {
            Artist artist = new Artist("Artist " + i);
            fillAlbums(artist.getAlbums());
            artists.add(artist);
        }
    }

    private void fillAlbums(List<Album> albums) {
        for (int i = 0; i < 2; i++) {
            albums.add(new Album("Album " + i, (2000 + i)));
        }
    }


}